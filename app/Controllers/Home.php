<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		/*$all_pekan = [
			[
				'nama' => 'Seremban',
				'gambar' => 'https://images.unsplash.com/photo-1610917125146-8536b0199df8?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8am9ob3IlMjBiYWhydXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium veritatis cupiditate nesciunt, quam fugiat maiores ducimus officia. Reprehenderit velit illum ratione. Sequi quia vitae ut beatae et ad veritatis earum.'
			],
			[
				'nama' => 'Gemas',
				'gambar' => 'https://images.unsplash.com/photo-1521317925431-c2256dd4fe2a?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MjZ8fGpvaG9yJTIwYmFocnV8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium veritatis cupiditate nesciunt, quam fugiat maiores ducimus officia. Reprehenderit velit illum ratione. Sequi quia vitae ut beatae et ad veritatis earum.'	
			],
			[
				'nama' => 'Kuala Pilah',
				'gambar' => 'https://images.unsplash.com/photo-1598966373020-11c31f342a49?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mjh8fGpvaG9yJTIwYmFocnV8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium veritatis cupiditate nesciunt, quam fugiat maiores ducimus officia. Reprehenderit velit illum ratione. Sequi quia vitae ut beatae et ad veritatis earum.'	
			],
			[
				'nama' => 'Juaseh',
				'gambar' => 'https://images.unsplash.com/photo-1521318140312-ac37e7adf220?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MjV8fGpvaG9yJTIwYmFocnV8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium veritatis cupiditate nesciunt, quam fugiat maiores ducimus officia. Reprehenderit velit illum ratione. Sequi quia vitae ut beatae et ad veritatis earum.'	
			]
		];*/
		$db = db_connect();
		$result = $db->query('SELECT * FROM gambar ORDER BY nama');
		$all_pekan = $result->getResult();
		return view('homepage', ['all_pekan' => $all_pekan]);
	}

	public function hello()
	{
		echo "<h1>Hello...</h1>";
	}

	public function welcome()
	{
		echo "<h1>Welcome...</h1>";
	}
}
